﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NAudio;
using NAudio.Wave;
using maper;
using WMPLib;

namespace AI
{
    class FMaper
    {
        public bool [,] FFMAP;
        public int length;
        /// <summary>
        /// Crates empty map.
        /// </summary>
        public FMaper(int length)
        {
            this.length = length;
            for(int i = 0; i < length; i++)
            {
                for(int j = 0; j < 6; j++)
                {
                    this.FFMAP[i, j] = false;
                }
            }
        }
        /// <summary>
        /// Addition row to map.
        /// </summary>
        public FMaper(bool [] row, FMaper map)
        {
            this.length = (map.length + 1);
            for (int i = 0; i < map.length; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    this.FFMAP[i, j] = map.FFMAP[i,j];
                }
            }
            for (int j = 0; j < 6; j++)
            {
                this.FFMAP[map.length, j] = row[j];
            }
        }

        public static int PlayFmap(string songname)
        {
            Console.Clear();
            Console.SetWindowSize(25, 32);
            Console.SetBufferSize(25, 32);
            Mp3FileReader reader = new Mp3FileReader(songname);
            Mp3Frame frame;
            int counter = 0;
            int k = 0;
            map FMap = new map();
            WindowsMediaPlayer wmp = new WindowsMediaPlayer();
            while ((frame = reader.ReadNextFrame()) != null)
            {
                if (counter % 10 == 0)
                {
                    int[] sample = new int[6];
                    for (int i = 0; i < 36; i++)
                    {
                        sample[i / 6] += frame.RawData[i];
                    }
                    FMap = map.move(FMap, Array.IndexOf(sample, sample.Max()) * 4 + 2);
                    k += map.MapPlay(FMap, 260);
                }
                if (counter == 300)
                {
                    wmp.URL = @"test.mp3";
                    wmp.controls.play();
                }
                counter++;
            }
            for(int addtime = 0; addtime<30; addtime++)
            {
                FMap = map.empty(FMap);
                k += map.MapPlay(FMap, 260);
            }
            wmp.controls.stop();
            Console.SetWindowSize(44, 32);
            Console.SetBufferSize(44, 32);
            return k;
        }
    }

}
