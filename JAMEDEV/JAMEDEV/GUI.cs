﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Console;
using System.IO;
using maper;
using AI;


namespace JAMEDEV
{
    public class GUI
    {
        int position;
        
        public GUI()
            {
                this.position = 1;
            }
        protected static void WriteAt(string s, int x, int y)
        {
            
                SetCursorPosition(x,y);
                Write(s);
            
        }
        public static void  playmenu()
        {
            SetWindowSize(44, 32);
            SetBufferSize(44, 32);
            Cursor.Hide();
            bool menu = true;
            int position = 1;
            string path = Application.ExecutablePath;
            path = path.Remove(path.Length - 12);
            path = path + "\\GUI.txt";
            string text = System.IO.File.ReadAllText(@path);
            WriteLine(text);
            ForegroundColor = ConsoleColor.DarkCyan;
            int CursorPos = 4;
            WriteAt("►", 9, CursorPos);
            CursorVisible=false;
            while (menu)
            {
                switch (ReadKey(true).Key)
                {
                    case ConsoleKey.UpArrow:

                        if (position > 1)
                        {
                            position--;
                            WriteAt(" ", 9, CursorPos);
                            CursorPos -= 2;
                            WriteAt("►", 9, CursorPos);
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        if (position < 5)
                        {
                            position++;
                            WriteAt(" ", 9, CursorPos);
                            CursorPos += 2;
                            WriteAt("►", 9 , CursorPos);
                        }
                            break;
                    case ConsoleKey.Enter:
                        switch (position)
                        {
                            case 1:
                                map Test = new map();
                                int k = map.MapPlay(Test, 0);
                                Random rnd = new Random();
                                byte[] b = new Byte[600];
                                rnd.NextBytes(b);
                                SetWindowSize(25, 32);
                                SetBufferSize(25, 32);
                                for (int i = 0; i < 100; i++)
                                {
                                    Test = map.move(Test, b[i] % 6 * 4 + 2);
                                    k += map.MapPlay(Test, 350);
                                }
                                
                                Clear();
                                while (ReadKey(true).Key != ConsoleKey.Enter) { }
                                ForegroundColor = ConsoleColor.White;
                                WriteLine(k);
                                WriteLine("Wrie your name Hero!");
                                Record(k);
                                Clear();
                                SetWindowSize(44, 32);
                                SetBufferSize(44, 32);
                                WriteLine(text);
                                ForegroundColor = ConsoleColor.DarkCyan;
                                CursorPos = 4;
                                WriteAt("►", 9, CursorPos);
                                break;
                            case 2:
                                int result = FMaper.PlayFmap("test.mp3"); Clear();
                                while (ReadKey(true).Key != ConsoleKey.Enter) { }
                                ForegroundColor = ConsoleColor.White;
                                WriteLine("Write Your Name!!");
                                Record(result);
                                Clear();
                                ForegroundColor = ConsoleColor.White;
                                WriteLine(text);
                                ForegroundColor = ConsoleColor.DarkCyan;
                                CursorPos = 4;
                                WriteAt("►", 9, CursorPos);
                                position = 1;
                                break;
                            case 3:
                                string RecordPath = Application.ExecutablePath;
                                RecordPath = RecordPath.Remove(RecordPath.Length - 12);
                                RecordPath = RecordPath + "\\records.txt";
                                string records = File.ReadAllText(@RecordPath);
                                Clear();
                                ForegroundColor = ConsoleColor.White;
                                WriteLine(records);
                                while (ReadKey(true).Key != ConsoleKey.Escape) { }
                                Clear();
                                WriteLine(text);
                                ForegroundColor = ConsoleColor.DarkCyan;
                                CursorPos = 4;
                                WriteAt("►", 9, CursorPos);
                                position = 1;
                                break;
                            case 4:
                                Clear();
                                ForegroundColor = ConsoleColor.Yellow;
                                string credits = File.ReadAllText(@"Credits.txt");
                                WriteLine(credits);
                                ReadKey();
                                Clear();
                                ForegroundColor = ConsoleColor.White;
                                WriteLine(text);
                                ForegroundColor = ConsoleColor.DarkCyan;
                                CursorPos = 4;
                                WriteAt("►", 9, CursorPos);
                                position = 1;
                                break;    
                            case 5:
                                menu = false;
                                break;
                        }
                              
                        break;
                }

                }
            }


        public static void Record(int Score)
        {
            int[] Records = new int[10];
            string[] RecordName = new string[10];
            StreamReader file = new StreamReader(@"records.txt");
            int counter = 0;
            string line;
            while ((line = file.ReadLine()) != null)
            {
                int k = line.LastIndexOf(' ');
                RecordName[counter] = line;
                try { string numb = line.Remove(0, k);
                Records[counter] = Int32.Parse(numb); }
                catch (Exception) { }
                
                counter++;
            }
            int i = 9;
            try
            {
                while (Score > Records[i-1]&&i>0)
                {
                    i--;
                }
            }
            catch (IndexOutOfRangeException)
            {
                i++;
            }
            for (int j=9; j>i; j--)
            {
                Records[j] = Records[j -1];
                RecordName[j] = RecordName[j-1];
            }
            string heroname = ReadLine();
            if (String.IsNullOrEmpty(heroname))
                heroname = "Unnamed";
            RecordName[i] = heroname+" "+Score;

            file.Close();
            File.WriteAllText(@"records.txt", string.Empty);
            StreamWriter writer = new StreamWriter(@"records.txt");
            foreach (string Name in RecordName)
            {
                writer.WriteLine(Name);
            }
            writer.Close();
        }
        }
    }

