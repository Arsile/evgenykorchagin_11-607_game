﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MouseKeyboardStateTest;
using System.Windows.Forms;


namespace maper
{
    public class map
    {
        public char[,] level = new char [30,24];
        public int[] position = new int[30];
        public map()
        {
            for (int i = 0; i<30; i++)
            {
                for (int j = 0; j < 24; j++)
                {
                    if (j % 4 == 2)
                    {
                        level[i, j] = '@';
                    }
                    else
                    {
                        level[i, j] = ' ';
                    }
                }
            }
        }
        public map(int k)
        {
            if (k == 0)
            {
                for (int i = 0; i < 30; i++)
                {
                    for (int j = 0; j < 24; j++)
                    {
                        level[i, j] = ' ';
                    }
                }
            }
        }
        public static map move(map Map, int pos)
        {
            map result = new map(0);
            for (int i = 29; i > 0; i--)
            {
                for (int j = 0; j < 24; j++)
                {
                    result.level[i-1, j] = Map.level[i, j];
                }
                result.position[i-1] = Map.position[i];
            }
            for (int j = 0; j < 24; j++)
            {
                result.level[29, j] = ' ';
            }   
            result.level[29, pos] = '@';
            result.position[29] = pos;
            return result;
        }
        public static map empty(map Map)
        {
            map result = new map(0);
            for (int i = 29; i > 0; i--)
            {
                for (int j = 0; j < 24; j++)
                {
                    result.level[i - 1, j] = Map.level[i, j];
                }
                result.position[i - 1] = Map.position[i];
            }
            for (int j = 0; j < 24; j++)
            {
                result.level[29, j] = ' ';
            }
            result.position[29] = 0;
            return result;
        }
        /// <summary>
        /// Plays current Map with delay, returns points from it.
        /// </summary>
        public static int MapPlay(map Map,int delay)
        {
            for (int i = 0; i < 30; i++)
            {
                for (int j = 0; j < 24; j++)
                {
                    if (i == 2)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                    switch (j/4)
                    {
                        case 0:
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write(Map.level[i, j]);
                            break;
                        case 1:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write(Map.level[i, j]);
                            break;
                        case 2:
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Write(Map.level[i, j]);
                            break;
                        case 3:
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write(Map.level[i, j]);
                            break;
                        case 4:
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            Console.Write(Map.level[i, j]);
                            break;
                        case 5:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write(Map.level[i, j]);
                            break;
                    }
                }
                Console.WriteLine();
            }
            bool before = KeyCheck(Map.position[2]);
            System.Threading.Thread.Sleep(delay);
            bool after = KeyCheck(Map.position[2]);
            if (after||before)
            {
                Console.Clear();
                return 1;
            }
            else
            {
                Console.Clear();
                return 0;
            }
        }
        public static bool KeyCheck(int position)
        {
            switch (position / 4)
            {
                case 0:
                    return Keyboard.IsKeyDown(Keys.F);
                case 1:
                    return Keyboard.IsKeyDown(Keys.G);
                case 2:
                    return Keyboard.IsKeyDown(Keys.H);
                case 3:
                    return Keyboard.IsKeyDown(Keys.J);
                case 4:
                    return Keyboard.IsKeyDown(Keys.K);
                case 5:
                    return Keyboard.IsKeyDown(Keys.L);
                default:
                    return false;
            }
        }
    }
}
